#pragma once

#include "backtest.h"
#include <string>
#include <vector>

std::string get_strategy_report(const std::vector<backtest_t> &);
